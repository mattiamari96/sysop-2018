#!/bin/bash

NUM=$1
if [[ $NUM > 1 ]]; then
	(( RIS *= NUM ))
	(( NUM -= 1 ))
	source ./fattoriale1.sh $NUM
fi
