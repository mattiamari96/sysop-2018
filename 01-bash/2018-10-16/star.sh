#!/bin/bash

while read -r line; do
	line=`echo "$line" | cut -d" " -f3,4 -`
	IFS=' ' read -r -a words <<< "$line"
	echo "${words[1]}" "${words[0]}"
done
