#!/bin/bash

N=10

while [[ $N -gt 0 ]]; do
	./puntini.sh 5 >&2 &
	echo -n "$! "
	((N-=1))
done
