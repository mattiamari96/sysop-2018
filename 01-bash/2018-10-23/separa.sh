#!/bin/bash

RES="$PATH"

while [[ -n "$RES" ]]; do
	P=${RES%%:*}
	echo "$P"
	RES=${RES#*:}
	if [[ $P == $RES  ]]; then break; fi
done
