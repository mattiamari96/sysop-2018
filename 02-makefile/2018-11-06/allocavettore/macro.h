#ifndef MACRO_H
#define MACRO_H

#define ALLOCAVETT(PTR) \
    PTR = (int32_t*)malloc(10 * sizeof(int32_t)); \
    if (PTR != NULL) { \
        int i; \
        for (i = 0; i < 10; i++) { \
            PTR[i] = -1000 + i; \
        } \
    }


#endif
