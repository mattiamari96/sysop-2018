#ifndef DIMEZZA_H
#define DIMEZZA_H

/* implementa una funzione che prende un argomento double e
   restituisce un double pari alla meta’ del COSENO dell’argomento passato
*/
double dimezza(double n);

#endif
