#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#define NUM_THREADS 10

pthread_t tid[NUM_THREADS];

void *worker(void *arg) {
	printf("%lu - received: %d\n", pthread_self(), *((int *)arg));
	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	srand(time(NULL));
	
	for (int i = 0; i < NUM_THREADS; i++) {
		int *r = (int *)malloc(sizeof(int));
		*r = rand() % 100;

		pthread_create(&tid[i], NULL, worker, (void *)r);
	}
	
	for (int i = 0; i < NUM_THREADS; i++) {
		pthread_join(tid[i], NULL);
	}

	return 0;
}
