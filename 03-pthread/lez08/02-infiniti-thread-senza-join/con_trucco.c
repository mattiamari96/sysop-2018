/* banale_contrucco.c  */

/* messi nella riga di comando del compilatore 
#define _THREAD_SAFE
#define _REENTRANT
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>	/* uintptr_t */
#include <inttypes.h>	/* PRIiPTR */
#include <pthread.h>


#define NUM_THREADS 5

int N=-1;

void *PrintHello(void *arg)
{
	sleep(1);
	printf("\n %"  PRIiPTR ": Hello World! N=%" PRIiPTR "\n", (intptr_t)arg, (intptr_t)arg );
	pthread_exit(NULL);
}

int main()
{
	pthread_t tid;
	int rc;
	intptr_t t;
	char err[256];
	
	for(t=0; 1; t++){
		printf("Creating thread %" PRIiPTR "\n", t);
		rc = pthread_create(&tid, NULL, PrintHello, (void*)t);
		if (rc){
			printf("ERROR; return code from pthread_create() is %i \n",rc);
			strerror_r(rc, err, 256);
			printf("%s\n", err);
			exit(1);
		}
	}

	pthread_exit(NULL);  /* far vedere commentando questa riga */
	return(0);
}
