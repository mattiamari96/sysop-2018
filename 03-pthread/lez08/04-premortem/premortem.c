#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

typedef struct threadargs_s {
	int num;
	pthread_t tid;
} threadargs;

void *luke(void *arg) {
	threadargs *inargs = (threadargs *)arg;
	threadargs *outargs = (threadargs *)malloc(sizeof(threadargs));
	pthread_t tid;
	
	printf("num: %d\n", inargs->num);
	usleep(1000);
	outargs->num = inargs->num + 1;
	outargs->tid = pthread_self();
	pthread_create(&tid, NULL, luke, (void *)outargs);
	
	pthread_join(inargs->tid, NULL);
	free(arg);
	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	threadargs *targs = (threadargs *)malloc(sizeof(threadargs));
	int terr;
	char buf[256];
	pthread_t tid;
	
	usleep(1000);
	targs->num = 1;
	targs->tid = pthread_self();
	terr = pthread_create(&tid, NULL, luke, (void *)targs);

	if (terr) {
		strerror_r(terr, buf, 256);
		printf("%s\n", buf);
		exit(1);
	}
	
	pthread_exit(NULL);
	
	return 0;
}
