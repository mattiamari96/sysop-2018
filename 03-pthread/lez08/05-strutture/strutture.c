#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

#define START_THREADS 4

typedef struct _Struttura {
	int N;
	char Str[100];
	int Indice;
} Struttura;

Struttura *Struttura_alloc() {
	return (Struttura *)malloc(sizeof(Struttura));
}

void *worker(void *arg) {
	Struttura *in;
	int i;
	pthread_t *tids;
	Struttura *out;
	
	sleep(1);
	in = (Struttura *)arg;
	tids = (pthread_t *)malloc(in->N * sizeof(pthread_t));
	
	if (in->N > 1) {
		for (i = 0; i < in->N - 1; i++) {
			out = Struttura_alloc();
			out->N = in->N - 1;
			out->Indice = i;
			pthread_create(&tids[i], NULL, worker, (void *)out);
		}
		
		for (i = 0; i < in->N - 1; i++) {
			pthread_join(tids[i], (void **)&out);
			if (out != NULL && out->Str != NULL) {
				printf("%s\n", out->Str);
			}
		}
	}
	
	sprintf(in->Str, "%d %d", in->N, in->Indice);
	pthread_exit((void *)in);
}

int main(int argc, char **argv) {
	int i;
	Struttura *out;
	pthread_t tids[START_THREADS];
	
	for (i = 0; i < START_THREADS; i++) {
		out = Struttura_alloc();
		out->N = START_THREADS;
		out->Indice = i;
		pthread_create(&tids[i], NULL, worker, (void *)out);
	}
	
	for (i = 0; i < START_THREADS; i++) {
		pthread_join(tids[i], NULL);
	}
	
	pthread_exit(NULL);
	
	return 0;
}
