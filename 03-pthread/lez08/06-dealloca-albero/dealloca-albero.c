#include <stdlib.h>
#include <stdio.h>

#define NUM_NODES 2000

typedef struct s_Node {
	int key;
	double x;
	struct s_Node *left;
	struct s_Node *right;
} Node;

Node *Node_alloc() {
	Node *out;
	
	out = (Node *)malloc(sizeof(Node));
	out->key = 0;
	out->left = NULL;
	out->right = NULL;
	
	return out;
}

void tree_insert(double x, Node **N) {
	if (*N == NULL) {
		*N = Node_alloc();
		(*N)->x = x;
		return;
	}
	if ((*N)->x == x) {
		return;
	}
	if (x < (*N)->x) {
		tree_insert(x, &((*N)->left));
	} else {
		tree_insert(x, &((*N)->right));
	}
}

void tree_build(Node **root) {
	int i;
	
	for (i = 0; i < NUM_NODES; i++) {
		tree_insert((double)i, root);
	}
}

void tree_dealloc(Node **root) {
	if (*root == NULL) {
		return;
	}
	
	tree_dealloc(&((*root)->left));
	tree_dealloc(&((*root)->right));
	free(*root);
	root = NULL;
}

void tree_use(Node *N) {
	if (N == NULL) {
		return;
	}
	
	tree_use(N->left);
	printf("%f\n", N->x);
	tree_use(N->right);
}

int main(int argc, char **argv) {
	Node *root;
	
	root = NULL;
	
	tree_build(&root);
	tree_use(root);
	tree_dealloc(&root);
	
	return 0;
}
