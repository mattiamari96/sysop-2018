#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

#define N_TRD_DEPOSITO 5
#define N_TRD_PRELIEVO 4
#define N_BANCHE 3
#define PRINT_EVERY 3

void *deposito(void *args);
void *prelievo(void *args);
void *bancaitalia(void *args);

void printonerror(int retval, short exit);

int T[N_BANCHE];
int N[N_BANCHE];
pthread_mutex_t mtx;
int n_prints;

int main(int argc, char **argv) {
	int i;
	int *n;
	pthread_t tid[N_TRD_DEPOSITO + N_TRD_PRELIEVO];
	pthread_t tid_bancaitalia;
	
	for (i = 0; i < N_BANCHE; i++) {
		T[i] = 0;
		N[i] = 0;
	}
	
	printonerror(pthread_mutex_init(&mtx, NULL), 1);
	
	for (i = 0; i < N_TRD_DEPOSITO; i++) {
		n = malloc(sizeof(int));
		*n = i % N_BANCHE;
		printonerror(pthread_create(&tid[i], NULL, deposito, (void *)n), 0);
	}
	
	for (i = 0; i < N_TRD_PRELIEVO; i++) {
		n = malloc(sizeof(int));
		*n = i % N_BANCHE;
		printonerror(pthread_create(&tid[N_TRD_DEPOSITO + i], NULL, prelievo, (void *)n), 0);
	}
	
	printonerror(pthread_create(&tid_bancaitalia, NULL, bancaitalia, NULL), 1);
	
	for (i = 0; i < N_TRD_DEPOSITO + N_TRD_PRELIEVO; i++) {
		pthread_join(tid[i], NULL);
	}
	
	pthread_join(tid_bancaitalia, NULL);
	
	pthread_exit(NULL);
}

void *deposito(void *args) {
	int n = *(int *)args;
	
	while (1) {
		sleep(1);
		
		printonerror(pthread_mutex_lock(&mtx), 1);
		T[n] += 10;
		N[n]++;
		usleep(100000);
		printonerror(pthread_mutex_unlock(&mtx), 1);
	}
	
	pthread_exit(NULL);
}

void *prelievo(void *args) {
	int n = *(int *)args;
	
	while (1) {
		sleep(1);
		
		printonerror(pthread_mutex_lock(&mtx), 1);
		T[n] -= 9;
		N[n]++;
		usleep(100000);
		printonerror(pthread_mutex_unlock(&mtx), 1);
	}
	
	pthread_exit(NULL);
}

void *bancaitalia(void *args) {
	int i, t, n;
	while (1) {
		printonerror(pthread_mutex_lock(&mtx), 1);
		
		t = 0;
		n = 0;
		for (i = 0; i < N_BANCHE; i++) {
			t += T[i];
			n += N[i];
		}
		
		printf("%ds    Tot: %d    Op: %d\n", n_prints * PRINT_EVERY, t, n);
		sleep(1);
		printonerror(pthread_mutex_unlock(&mtx), 1);
		
		n_prints++;
		sleep(PRINT_EVERY);
	}
	
	pthread_exit(NULL);
}

void printonerror(int retval, short exit) {
	char buf[256];
	
	if (retval == 0)
		return;
	
	strerror_r(retval, buf, 256);
	printf("%s\n", buf);
	
	if (exit)
		pthread_exit(NULL);
}
