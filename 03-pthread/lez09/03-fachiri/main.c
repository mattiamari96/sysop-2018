#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

#define N_FACHIRI 2
#define N_SPADE 10

void *fachiro(void *args);

void printonerror(int retval, short exit);

pthread_mutex_t mtx_spada[N_SPADE];
char madonne[N_SPADE][128] = {
	"ahia!",
	"porca paletta",
	"porca puttana",
	"porca ladonna",
	"zio cane",
	"zio stronzo",
	"zio scalzo nella valle dei chiodi",
	"ladonna inculata da un cane",
	"zio cane zio gatto zio pulcino zio maiale zio cinghiale",
	"zio frocio zio belva zio maiale scotennato"
};

int main(int argc, char **argv) {
	int i;s
	pthread_t tid[N_FACHIRI];
	
	for (i = 0; i < N_SPADE; i++) {
		printonerror(pthread_mutex_init(&mtx_spada[i], NULL), 1);
	}
	
	for (i = 0; i < N_FACHIRI; i++) {
		printonerror(pthread_create(&tid[i], NULL, fachiro, NULL), 0);
	}
	
	for (i = 0; i < N_FACHIRI; i++) {
		pthread_join(tid[i], NULL);
	}
	
	pthread_exit(NULL);
}

void *fachiro(void *args) {
	int i;
	
	while (1) {
		for (i = 0; i < N_SPADE; i++) {
			printonerror(pthread_mutex_lock(&mtx_spada[i]), 1);
			usleep(100000);
			printf("%lu   %s\n", pthread_self(), madonne[i]);
		}
		
		for (i = 0; i < N_SPADE; i++) {
			printonerror(pthread_mutex_unlock(&mtx_spada[i]), 1);
		}
	}
	
	pthread_exit(NULL);
}

void printonerror(int retval, short exit) {
	char buf[256];
	
	if (retval == 0)
		return;
	
	strerror_r(retval, buf, 256);
	printf("%s\n", buf);
	
	if (exit)
		pthread_exit(NULL);
}
