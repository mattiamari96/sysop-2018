#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#define PASSEGGERI 3
#define MAX_PASSEGGERI 2

void *dino(void *args);
void *passeggero(void *args);

int passeggeri_su_coda;
short lato_coda;
pthread_mutex_t mutex_salita_discesa;
pthread_cond_t cond_pieno;
pthread_cond_t cond_vuoto;
pthread_cond_t cond_arrivato;

void *dino(void *args) {
	while (1) {
		pthread_mutex_lock(&mutex_salita_discesa);
		pthread_cond_wait(&cond_pieno, &mutex_salita_discesa);
		sleep(2);
		lato_coda = (lato_coda + 1) % 2;
		printf("[ dino ] lato: %d\n", lato_coda);
		pthread_cond_broadcast(&cond_arrivato);
		pthread_mutex_unlock(&mutex_salita_discesa);
	}
	
	pthread_exit(NULL);
}

void *passeggero(void *args) {
	short lato;
	short su_coda;
	
	/*lato = rand() % 2;*/
	lato = *(short *)args;
	su_coda = 0;
	
	while (1) {
		pthread_mutex_lock(&mutex_salita_discesa);
		
		if (!su_coda && lato == lato_coda && passeggeri_su_coda < MAX_PASSEGGERI) {
			su_coda = 1;
			passeggeri_su_coda++;
			printf("Salita. Su coda: %d\n", passeggeri_su_coda);
		}
		if (passeggeri_su_coda == MAX_PASSEGGERI) {
			pthread_cond_signal(&cond_pieno);
		}
		
		pthread_cond_wait(&cond_arrivato, &mutex_salita_discesa);
		
		if (su_coda) {
			su_coda = 0;
			passeggeri_su_coda--;
			printf("Discesa. Su coda: %d\n", passeggeri_su_coda);
			lato = lato_coda;
		}
		
		pthread_mutex_unlock(&mutex_salita_discesa);
		sleep(4);
	}

	pthread_exit(NULL);
}

int main(int argc, char **argv) {
	int i;
	pthread_t dino_tid;
	pthread_t passeggeri_tid[PASSEGGERI];
	short passeggeri_lato[PASSEGGERI] = {0, 0, 1};
	short *arg;
	
	srand(time(NULL));
	
	passeggeri_su_coda = 0;
	lato_coda = 0;
	
	pthread_mutex_init(&mutex_salita_discesa, NULL);
	pthread_cond_init(&cond_pieno, NULL);
	pthread_cond_init(&cond_vuoto, NULL);
	
	pthread_create(&dino_tid, NULL, dino, NULL);
	
	for (i = 0; i < PASSEGGERI; i++) {
		arg = malloc(sizeof(short));
		*arg = passeggeri_lato[i];
		pthread_create(&passeggeri_tid[i], NULL, passeggero, (void *)arg);
	}
	for (i = 0; i < PASSEGGERI; i++) {
		pthread_join(passeggeri_tid[i], NULL);
	}
	
	pthread_join(dino_tid, NULL);
	
	pthread_exit(NULL);
}
