#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/stat.h> /* For mode constants */
#include <fcntl.h> /* For O_* constants */

#define PASSEGGERI 3
#define MAX_PASSEGGERI 2

void dino();
void passeggero(short l);

typedef struct SharedState_s {
	int passeggeri_su_coda;
	short lato_coda;
	pthread_mutex_t mutex_salita_discesa;
	pthread_cond_t cond_pieno;
	pthread_cond_t cond_arrivato;
} SharedState;

SharedState *ST;

void dino() {
	while (1) {
		pthread_mutex_lock(&ST->mutex_salita_discesa);
		pthread_cond_wait(&ST->cond_pieno, &ST->mutex_salita_discesa);
		sleep(2);
		ST->lato_coda = (ST->lato_coda + 1) % 2;
		printf("[ dino ] lato: %d\n", ST->lato_coda);
		pthread_cond_broadcast(&ST->cond_arrivato);
		pthread_mutex_unlock(&ST->mutex_salita_discesa);
	}
}

void passeggero(short l) {
	short lato;
	short su_coda;
	
	lato = l;
	su_coda = 0;
	
	while (1) {
		pthread_mutex_lock(&ST->mutex_salita_discesa);
		
		if (!su_coda && lato == ST->lato_coda && ST->passeggeri_su_coda < MAX_PASSEGGERI) {
			su_coda = 1;
			ST->passeggeri_su_coda++;
			printf("Salita. Su coda: %d\n", ST->passeggeri_su_coda);
		}
		if (ST->passeggeri_su_coda == MAX_PASSEGGERI) {
			pthread_cond_signal(&ST->cond_pieno);
		}
		
		pthread_cond_wait(&ST->cond_arrivato, &ST->mutex_salita_discesa);
		
		if (su_coda) {
			su_coda = 0;
			ST->passeggeri_su_coda--;
			printf("Discesa. Su coda: %d\n", ST->passeggeri_su_coda);
			lato = ST->lato_coda;
		}
		
		pthread_mutex_unlock(&ST->mutex_salita_discesa);
		sleep(4);
	}
}

int main(int argc, char **argv) {
	int i;
	short passeggeri_lato[PASSEGGERI] = {0, 0, 1};
	int sfd;
	int pid;
	pthread_mutexattr_t mutexattr;
	pthread_condattr_t condattr;
	
	srand(time(NULL));
	
	sfd = shm_open("/flinstones", O_CREAT | O_RDWR, S_IRWXU);
	ftruncate(sfd, sizeof(SharedState));
	
	ST = (SharedState *)mmap(NULL, sizeof(SharedState),
		PROT_READ | PROT_WRITE, MAP_SHARED, sfd, 0);
	
	ST->passeggeri_su_coda = 0;
	ST->lato_coda = 0;
	
	pthread_mutexattr_init(&mutexattr);
	pthread_mutexattr_setpshared(&mutexattr, PTHREAD_PROCESS_SHARED);
	pthread_mutex_init(&ST->mutex_salita_discesa, &mutexattr);
	
	pthread_condattr_init(&condattr);
	pthread_condattr_setpshared(&condattr, PTHREAD_PROCESS_SHARED);
	pthread_cond_init(&ST->cond_pieno, &condattr);
	pthread_cond_init(&ST->cond_arrivato, &condattr);
	
	for (i = 0; i < PASSEGGERI; i++) {
		pid = fork();
		if (pid < 0) {
			printf("Fork error\n");
			return 1;
		}
		if (pid == 0) {
			passeggero(passeggeri_lato[i]);
			exit(0);
		}
	}

	dino();
	return 0;
}
