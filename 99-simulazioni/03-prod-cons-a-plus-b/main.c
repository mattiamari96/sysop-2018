#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <stdint.h>

#define N_PROD_A 3
#define N_PROD_B 5
#define N_CONS 10
#define BUF_LEN 64

void *prod_a(void *args);
void *prod_b(void *args);
void *consumer(void *args);
void printerror(int err);
void join_all(pthread_t *tids, int count);

uint64_t buf_a;
uint64_t buf_b;

pthread_mutex_t mtx;
pthread_cond_t data_ready;
short new_data_a, new_data_b;

int main(int argc, char **argv) {
	pthread_t tid_prod_a[N_PROD_A];
	pthread_t tid_prod_b[N_PROD_B];
	pthread_t tid_consumer;
	int i;
	int *trd_arg;
	
	new_data_a = 0;
	new_data_b = 0;
	
	printerror(pthread_mutex_init(&mtx, NULL));
	printerror(pthread_cond_init(&data_ready, NULL));
	
	for (i = 0; i < N_PROD_A; i++) {
		trd_arg = malloc(sizeof(int));
		*trd_arg = i;
		printerror(pthread_create(&tid_prod_a[i], NULL, prod_a, trd_arg));
	}
	for (i = 0; i < N_PROD_B; i++) {
		trd_arg = malloc(sizeof(int));
		*trd_arg = i;
		printerror(pthread_create(&tid_prod_b[i], NULL, prod_b, trd_arg));
	}
	printerror(pthread_create(&tid_consumer, NULL, consumer, NULL));
	
	join_all(tid_prod_a, N_PROD_A);
	join_all(tid_prod_b, N_PROD_B);
	printerror(pthread_join(tid_consumer, NULL));
	
	pthread_exit(NULL);
}

void *prod_a(void *args) {
	while(1) {
		sleep(1);
		pthread_mutex_lock(&mtx);
		
		buf_a = rand() % 30;
		new_data_a = 1;
		pthread_cond_broadcast(&data_ready);
		
		pthread_mutex_unlock(&mtx);
	}
}

void *prod_b(void *args) {
	while(1) {
		sleep(1);
		pthread_mutex_lock(&mtx);
		
		buf_b = rand() % 20;
		new_data_b = 1;
		pthread_cond_broadcast(&data_ready);
		
		pthread_mutex_unlock(&mtx);
	}
}

void *consumer(void *args) {
	while(1) {
		sleep(1);
		pthread_mutex_lock(&mtx);
		
		while (!new_data_a || !new_data_b) {
			pthread_cond_wait(&data_ready, &mtx);
		}
		
		printf("[ CONSUMER ] %lu\n", buf_a + buf_b);
		new_data_a = 0;
		new_data_b = 0;
		
		pthread_mutex_unlock(&mtx);
	}
}

void join_all(pthread_t *tids, int count) {
	int i;
	
	for (i = 0; i < count; i++) {
		printerror(pthread_join(tids[i], NULL));
	}
}

void printerror(int err) {
	char buf[256];
	
	if (err == 0) return;
	
	strerror_r(err, buf, 256);
	printf("[ FATAL ] %s\n", buf);
	pthread_exit(NULL);
}
