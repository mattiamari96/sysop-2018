#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

#define N_CARS_CW 10
#define N_CARS_ACW 8
#define N_WAYS 2

typedef struct CarData_s {
	short way; /* 0 = clockwise, 1 = anti-clockwise */
	int ticket;
} CarData;

void exitonerror(int err);
void *car(void *args);
void car_log(char *msg, CarData *data);

pthread_mutex_t mtx_bridge;
pthread_cond_t bridge_available;

int ticket_machine_n[N_WAYS];
int last_ticket[N_WAYS];
int queue_length[N_WAYS];
pthread_mutex_t ticket_machine_mtx[N_WAYS];

int main(int argc, char **argv) {
	int i;
	pthread_t car_cw_tid[N_CARS_CW];
	pthread_t car_acw_tid[N_CARS_ACW];
	CarData *car_data;
	
	pthread_mutex_init(&mtx_bridge, NULL);
	pthread_cond_init(&bridge_available, NULL);
	
	for (i = 0; i < N_WAYS; i++) {
		ticket_machine_n[i] = 1;
		last_ticket[i] = 0;
		queue_length[i] = 0;
		pthread_mutex_init(&ticket_machine_mtx[i], NULL);
	}
	
	for (i = 0; i < N_CARS_CW; i++) {
		car_data = malloc(sizeof(CarData));
		car_data->way = 0;
		pthread_create(&car_cw_tid[i], NULL, car, car_data);
	}
	for (i = 0; i < N_CARS_ACW; i++) {
		car_data = malloc(sizeof(CarData));
		car_data->way = 1;
		pthread_create(&car_acw_tid[i], NULL, car, car_data);
	}
	
	for (i = 0; i < N_CARS_CW; i++) {
		pthread_join(car_cw_tid[i], NULL);
	}
	for (i = 0; i < N_CARS_ACW; i++) {
		pthread_join(car_acw_tid[i], NULL);
	}
	
	pthread_exit(NULL);
}

void *car(void *args) {
	CarData *data;
	
	data = (CarData *)args;
	
	while (1) {
		/* get ticket */
		pthread_mutex_lock(&ticket_machine_mtx[data->way]);
		data->ticket = ticket_machine_n[data->way]++;
		queue_length[data->way]++;
		car_log("got ticket", data);
		pthread_mutex_unlock(&ticket_machine_mtx[data->way]);
		
		/* wait in line and get on the bridge */
		pthread_mutex_lock(&mtx_bridge);
		while(
			last_ticket[data->way] < data->ticket - 1
			|| queue_length[data->way] < queue_length[!data->way]
			|| (queue_length[data->way] == queue_length[!data->way] && data->way == 1)
		) {
			pthread_cond_wait(&bridge_available, &mtx_bridge);
		}
		last_ticket[data->way] = data->ticket;
		queue_length[data->way]--;
		car_log("got on bridge", data);
		sleep(1);
		pthread_cond_broadcast(&bridge_available);
		pthread_mutex_unlock(&mtx_bridge);
		car_log("got off bridge", data);
		
		sleep(5);
	}
	
	pthread_exit(NULL);
}

void exitonerror(int err) {
	char buf[256];
	
	if (err == 0) return;
	
	strerror_r(err, buf, 256);
	printf("[ ERROR ] %s\n", buf);
	pthread_exit(NULL);
}

void car_log(char *msg, CarData *data) {
	printf(
		"[ CAR %lu    way: %s    ticket: %d ] %s\n",
		pthread_self(),
		data->way == 0 ? "clockwise    " : "anticlockwise",
		data->ticket,
		msg
	);
	
	printf("queue CW: %d    queue ACW: %d\n", queue_length[0], queue_length[1]);
}
