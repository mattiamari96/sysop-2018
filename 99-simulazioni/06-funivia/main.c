#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

#define N_SOBRI 4
#define PASSEGGERI_MAX 2

typedef struct SharedState_s {
	pthread_mutex_t mtx;
	pthread_cond_t passeggero_sceso;
	pthread_cond_t passeggero_salito;
	pthread_cond_t discesa;
	pthread_cond_t salita;
	int passeggeri[PASSEGGERI_MAX];
	int passeggeri_count;
	short funivia_state; /* 0 = salita passeggeri, 1 = discesa passeggeri */
} SharedState;

void *funivia(void *args);
void *sobrio(void *args);
void exitonerror(int err, char *msg);

SharedState *ST;

int main(int argc, char **argv) {
	pthread_t funivia_tid;
	pthread_t passeggeri_tid[N_SOBRI];
	int i;
	int *n_pass;
	
	ST = (SharedState *)malloc(sizeof(SharedState));
	
	pthread_mutex_init(&ST->mtx, NULL);
	pthread_cond_init(&ST->passeggero_salito, NULL);
	pthread_cond_init(&ST->passeggero_sceso, NULL);
	pthread_cond_init(&ST->discesa, NULL);
	pthread_cond_init(&ST->salita, NULL);
	ST->passeggeri_count = 0;
	ST->funivia_state = -1;
	
	pthread_create(&funivia_tid, NULL, funivia, NULL);
	
	for (i = 0; i < N_SOBRI; i++) {
		n_pass = (int *)malloc(sizeof(int));
		*n_pass = i;
		pthread_create(&passeggeri_tid[i], NULL, sobrio, n_pass);
	}
	
	pthread_join(funivia_tid, NULL);
	for (i = 0; i < N_SOBRI; i++) {
		pthread_join(passeggeri_tid[i], NULL);	
	}
	
	return 0;
}

void *funivia(void *args) {
	int i;
	
	while (1) {
		pthread_mutex_lock(&ST->mtx);
		
		ST->funivia_state = 0; /* salita */
		pthread_cond_broadcast(&ST->salita);
		
		while (ST->passeggeri_count < PASSEGGERI_MAX) {
			pthread_cond_wait(&ST->passeggero_salito, &ST->mtx);	
		}
		
		ST->funivia_state = -1;
		
		for (i = 0; i < PASSEGGERI_MAX; i++) {
			printf("[ Posto %d | Pass. %d ] ", i, ST->passeggeri[i]);	
		}
		printf("\n");
		
		pthread_mutex_unlock(&ST->mtx);
		
		sleep(1);
		sleep(1);
		
		pthread_mutex_lock(&ST->mtx);
		
		ST->funivia_state = 1; /* discesa */
		pthread_cond_broadcast(&ST->discesa);
		
		while (ST->passeggeri_count > 0) {
			pthread_cond_wait(&ST->passeggero_sceso, &ST->mtx);	
		}
		
		ST->funivia_state = -1;
		
		pthread_mutex_unlock(&ST->mtx);
	}
}

void *sobrio(void *args) {
	int num = *(int *)args;
	
	while (1) {
		pthread_mutex_lock(&ST->mtx);

		while (ST->funivia_state != 0 || ST->passeggeri_count == PASSEGGERI_MAX) {
			pthread_cond_wait(&ST->salita, &ST->mtx);	
		}
		ST->passeggeri[ST->passeggeri_count] = num;
		ST->passeggeri_count++;
		pthread_cond_signal(&ST->passeggero_salito);
	
		while (ST->funivia_state != 1) {
			pthread_cond_wait(&ST->discesa, &ST->mtx);
		}

		ST->passeggeri_count--;
		printf("[ Pass. %d ] ké merda d panorama sn indinnhiatoh. denunzia cuerela\n", num);
		pthread_cond_signal(&ST->passeggero_sceso);

		pthread_mutex_unlock(&ST->mtx);
	}
}

void exitonerror(int err, char *msg) {
	char buf[256];
	if (err == 0) return;
	strerror_r(err, buf, 256);
	printf("[ ERROR] %s\n", buf);
	pthread_exit(NULL);
}