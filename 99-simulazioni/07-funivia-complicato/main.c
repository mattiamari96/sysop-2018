#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */

#define N_SOBRI 4
#define N_UBRIACHI 2
#define PASSEGGERI_MAX 2

typedef struct SharedState_s {
	pthread_mutex_t mtx;
	pthread_cond_t passeggero_sceso;
	pthread_cond_t passeggero_salito;
	pthread_cond_t discesa;
	pthread_cond_t salita;
	int passeggeri[PASSEGGERI_MAX];
	int sobrio[PASSEGGERI_MAX];
	int passeggeri_count;
	short funivia_state; /* 0 = salita passeggeri, 1 = discesa passeggeri */
} SharedState;

void funivia();
void sobrio(int num);
void ubriaco(int num);
void exitonerror(int err, char *msg);

SharedState *ST;

int main(int argc, char **argv) {
	pthread_mutexattr_t mtx_attr;
	pthread_condattr_t cond_attr;
	int i;
	int shm;
	int pid;
	
	shm = shm_open("/funivia", O_RDWR | O_CREAT, S_IRWXU);
	ftruncate(shm, sizeof(SharedState));
	ST = (SharedState *)mmap(NULL, sizeof(SharedState), PROT_READ | PROT_WRITE, MAP_SHARED, shm, 0);
	
	pthread_mutexattr_init(&mtx_attr);
	pthread_mutexattr_setpshared(&mtx_attr, PTHREAD_PROCESS_SHARED);
	pthread_condattr_init(&cond_attr);
	pthread_condattr_setpshared(&cond_attr, PTHREAD_PROCESS_SHARED);
	
	pthread_mutex_init(&ST->mtx, &mtx_attr);
	pthread_cond_init(&ST->passeggero_salito, &cond_attr);
	pthread_cond_init(&ST->passeggero_sceso, &cond_attr);
	pthread_cond_init(&ST->discesa, &cond_attr);
	pthread_cond_init(&ST->salita, &cond_attr);
	ST->passeggeri_count = 0;
	ST->funivia_state = -1;
	
	for (i = 0; i < N_SOBRI; i++) {
		pid = fork();
		
		if (pid == 0) {
			sobrio(i);
			return 0;
		}
	}
	for (i = 0; i < N_UBRIACHI; i++) {
		pid = fork();
		
		if (pid == 0) {
			ubriaco(i);
			return 0;
		}
	}
	
	funivia();
	return 0;
}

void funivia() {
	int i;
	
	while (1) {
		pthread_mutex_lock(&ST->mtx);
		
		ST->funivia_state = 0; /* salita */
		pthread_cond_broadcast(&ST->salita);
		
		while (ST->passeggeri_count < PASSEGGERI_MAX) {
			pthread_cond_wait(&ST->passeggero_salito, &ST->mtx);
			if (!ST->sobrio[0]) {
				break;	
			}
		}
		
		ST->funivia_state = -1;
		
		for (i = 0; i < PASSEGGERI_MAX; i++) {
			printf("[ Posto %d | Pass. %d ] ", i, ST->passeggeri[i]);	
		}
		printf("\n");
		
		pthread_mutex_unlock(&ST->mtx);
		
		sleep(1);
		sleep(1);
		
		pthread_mutex_lock(&ST->mtx);
		
		ST->funivia_state = 1; /* discesa */
		pthread_cond_broadcast(&ST->discesa);
		
		while (ST->passeggeri_count > 0) {
			pthread_cond_wait(&ST->passeggero_sceso, &ST->mtx);	
		}
		
		for (i = 0; i < PASSEGGERI_MAX; i++) {
			ST->passeggeri[i] = -1;
		}
		
		ST->funivia_state = -1;
		
		pthread_mutex_unlock(&ST->mtx);
	}
}

void sobrio(int num) {
	while (1) {
		pthread_mutex_lock(&ST->mtx);

		while (
			ST->funivia_state != 0
			|| ST->passeggeri_count == PASSEGGERI_MAX
			|| (ST->passeggeri_count > 0 && !ST->sobrio[0])
		) {
			pthread_cond_wait(&ST->salita, &ST->mtx);	
		}
		ST->passeggeri[ST->passeggeri_count] = num;
		ST->sobrio[ST->passeggeri_count] = 1;
		ST->passeggeri_count++;
		pthread_cond_signal(&ST->passeggero_salito);
	
		while (ST->funivia_state != 1) {
			pthread_cond_wait(&ST->discesa, &ST->mtx);
		}

		ST->passeggeri_count--;
		printf("[ Pass. %d ] ké merda d panorama sn indinnhiatoh. denunzia cuerela\n", num);
		pthread_cond_signal(&ST->passeggero_sceso);

		pthread_mutex_unlock(&ST->mtx);
	}
}

void ubriaco(int num) {
	while (1) {
		pthread_mutex_lock(&ST->mtx);

		while (
			ST->funivia_state != 0
			|| ST->passeggeri_count == PASSEGGERI_MAX
			|| ST->passeggeri_count > 1
			|| (ST->passeggeri_count > 0)
		) {
			pthread_cond_wait(&ST->salita, &ST->mtx);	
		}
		ST->passeggeri[ST->passeggeri_count] = num;
		ST->sobrio[ST->passeggeri_count] = 0;
		ST->passeggeri_count++;
		pthread_cond_signal(&ST->passeggero_salito);
	
		while (ST->funivia_state != 1) {
			pthread_cond_wait(&ST->discesa, &ST->mtx);
		}

		ST->passeggeri_count--;
		printf("[ Pass. %d ] pAnOrAmA bElIsSsSiMoO tElO cOnIgLiO ... CoNsIgLiO\n", num);
		pthread_cond_signal(&ST->passeggero_sceso);

		pthread_mutex_unlock(&ST->mtx);
	}
}

void exitonerror(int err, char *msg) {
	char buf[256];
	if (err == 0) return;
	strerror_r(err, buf, 256);
	printf("[ ERROR] %s\n", buf);
	pthread_exit(NULL);
}