#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#define N_SOCC 2

void *professore(void *args);
void *soccorso(void *args);

pthread_mutex_t mtx;
pthread_cond_t cond_aiuto, cond_malato, socc_arrivato, socc_andato;
short malato;
int soccorritori;

int main(int argc, char **argv) {
	char *frase_esorcista, *frase_medico;
	pthread_t tid_prof, tid_esorcista, tid_medico;
	
	pthread_mutex_init(&mtx, NULL);
	pthread_cond_init(&cond_aiuto, NULL);
	pthread_cond_init(&cond_malato, NULL);
	pthread_cond_init(&socc_arrivato, NULL);
	pthread_cond_init(&socc_andato, NULL);
	
	soccorritori = 0;
	malato = 0;
	
	frase_esorcista = "Vade retro Satana";
	frase_medico = "qEsTa MeDiCiNa AnDrA' bEnE";
	pthread_create(&tid_prof, NULL, professore, NULL);
	pthread_create(&tid_esorcista, NULL, soccorso, frase_esorcista);
	pthread_create(&tid_medico, NULL, soccorso, frase_medico);
	
	pthread_join(tid_prof, NULL);
	pthread_join(tid_esorcista, NULL);
	pthread_join(tid_medico, NULL);
	
	pthread_exit(NULL);
}

void *professore(void *args) {
	while(1) {
		pthread_mutex_lock(&mtx);
		malato = N_SOCC; /* ci vogliono 2 soccorritori per guarirlo */
		printf("orca che male\n");
		pthread_cond_broadcast(&cond_aiuto);
		
		while (malato > 0 || soccorritori > 0) {
			pthread_cond_wait(&socc_andato, &mtx);
		}
		
		sleep(1);
		pthread_mutex_unlock(&mtx);
		
		printf("faccio lezione\n");
		sleep(4); /* lezione */
	}
}

void *soccorso(void *args) {
	char *frase;
	
	frase = (char *)args;
	
	while(1) {
		pthread_mutex_lock(&mtx);
		
		/* attendo la richiesta di aiuto */
		while (malato == 0) {
			pthread_cond_wait(&cond_aiuto, &mtx);
		}
		
		/* arrivo dal prof */
		soccorritori++;
		pthread_cond_broadcast(&socc_arrivato);
		
		/* attendo gli altri soccorritori */
		while (soccorritori < N_SOCC) {
			pthread_cond_wait(&socc_arrivato, &mtx);	
		}
		pthread_mutex_unlock(&mtx);
		
		printf("%s\n", frase);
		sleep(2);
		
		pthread_mutex_lock(&mtx);
		malato--;
		soccorritori--;
		pthread_cond_broadcast(&socc_andato);
		
		while (soccorritori > 0) {
			pthread_cond_wait(&socc_andato, &mtx);
		}
		
		pthread_mutex_unlock(&mtx);
	}
}