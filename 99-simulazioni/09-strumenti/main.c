#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

#define N_JOB_TYPES 2
#define N_L1 2
#define N_L2 2
#define N_TOOLS 3
#define TICK_DURATION 1000000
#define JOB_START_DELAY 1000000

#define DEBUG 0

void *job(void *args);

/* 1 = start using, -1 = stop using, 0 = do nothing */
short job_actions[2][3][5] = {

	{
		{1, 0, 0, 0, 0},
		{0, 1, 0, -1, 0},
		{0, 0, 0, 1, 0}
	},
	{
		{1, 0, 0, 0, 0},
		{0, 1, 0, -1, 0},
		{1, 0, 0, 0, 0}
	}
};

/*short job_actions[2][3][5] = {
	{
		{1, 0, 0, 0, 0},
		{1, 0, 0, 0, 0},
		{1, 0, 0, 0, 0}
	},
	{
		{1, 0, 0, 0, 0},
		{1, 0, 0, 0, 0},
		{1, 0, 0, 0, 0}
	}
};*/

int job_duration[N_JOB_TYPES] = {5, 4};
int tools_qty[N_TOOLS] = {2, 4, 2};

pthread_mutex_t mutex_tools;
pthread_cond_t cond_tools_changed;
short tools_used[N_TOOLS];
int jobs_done[N_JOB_TYPES];

int main(int argc, char **argv) {
	int i, elap;
	pthread_t tid_L1[N_L1];
	pthread_t tid_L2[N_L2];
	int *job_n;
	
	pthread_mutex_init(&mutex_tools, NULL);
	pthread_cond_init(&cond_tools_changed, NULL);
	
	for (i = 0; i < N_TOOLS; i++) {
		tools_used[i] = 0;
	}
	for (i = 0; i < N_JOB_TYPES; i++) {
		jobs_done[i] = 0;
	}
	
	for (i = 0; i < N_L1; i++) {
		job_n = malloc(sizeof(int));
		*job_n = 0; /* L1 */
		pthread_create(&tid_L1[i], NULL, job, job_n);
	}
	for (i = 0; i < N_L2; i++) {
		job_n = malloc(sizeof(int));
		*job_n = 1; /* L2 */
		pthread_create(&tid_L2[i], NULL, job, job_n);
	}
	
	for (elap = 0; 1; elap+=3) {
		pthread_mutex_lock(&mutex_tools);
		printf("%ds: Jobs 1: %d    Jobs 2: %d\n", elap, jobs_done[0], jobs_done[1]);
		pthread_mutex_unlock(&mutex_tools);
		sleep(3);
	}
	
	for (i = 0; i < N_L1; i++) {
		pthread_join(tid_L1[i], NULL);
	}
	for (i = 0; i < N_L2; i++) {
		pthread_join(tid_L2[i], NULL);
	}
	
	pthread_exit(NULL);
}

void *job(void *args) {
	int job_n;
	int i;
	int tick;
	int using[N_TOOLS];
	
	job_n = *(int *)args;
	
	for (i = 0; i < N_TOOLS; i++) {
		using[i] = 0;
	}
	
	for (tick = 0; 1; tick = (tick + 1) % job_duration[job_n]) {
		if (DEBUG && tick == 0) printf("[ %lu ] job %d started\n", pthread_self(), job_n+1);
			
		pthread_mutex_lock(&mutex_tools);
		
		for (i = 0; i < N_TOOLS; i++) {
			while (tools_used[i] + job_actions[job_n][i][tick] > tools_qty[i]) {
				if (DEBUG) printf("[ %lu ] [ tick %d ] [ job %d ] waiting tool %d\n", pthread_self(), tick, job_n+1, i+1);
				pthread_cond_wait(&cond_tools_changed, &mutex_tools);
			}
			
			tools_used[i] += job_actions[job_n][i][tick];
			using[i] += job_actions[job_n][i][tick];
		}
		
		pthread_cond_broadcast(&cond_tools_changed);
		pthread_mutex_unlock(&mutex_tools);
		
		usleep(TICK_DURATION); /* do work */
		
		if (tick == job_duration[job_n] - 1) {
			pthread_mutex_lock(&mutex_tools);
			for (i = 0; i < N_TOOLS; i++) {
				if (using[i]) {
					tools_used[i] -= 1;
					using[i] = 0;
				}
			}
			
			jobs_done[job_n]++;
			pthread_mutex_unlock(&mutex_tools);
			usleep(JOB_START_DELAY);
			if (DEBUG) printf("[ %lu ] job %d ended\n", pthread_self(), job_n+1);
		}
	}
	
	pthread_exit(NULL);
}
