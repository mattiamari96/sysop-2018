#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>

#define DEBUG_PRINT_DELAY 50000

#define HOUSE_CAP 2
#define ALIENS_OUTSIDE_MIN 3
#define ALIENS_NEW 2
#define ALIENS_INITIAL 5

void *alien(void *args);
void errexit(int err, char *msg);

int house_count;
int house_progressive;
int house_last_exit;
int outside_count;

pthread_mutex_t mtx;
pthread_cond_t cond_alien_exit;
pthread_cond_t cond_alien_enter;
pthread_attr_t attr;

int main(int argc, char **argv) {
	int i, err;
	pthread_t pid;
	
	house_count = 0;
	house_progressive = 0;
	house_last_exit = -1;
	outside_count = 0;
	
	err = pthread_mutex_init(&mtx, NULL);
	errexit(err, "");
	
	err = pthread_cond_init(&cond_alien_exit, NULL);
	errexit(err, "");
	
	err = pthread_cond_init(&cond_alien_enter, NULL);
	errexit(err, "");
	
	err = pthread_attr_init(&attr);
	errexit(err, "");
	pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
	
	err = pthread_mutex_lock(&mtx);
	errexit(err, "");
	
	for (i = 0; i < ALIENS_INITIAL; i++) {
		err = pthread_create(&pid, &attr, alien, NULL);
		errexit(err, "");
	}
	
	err = pthread_mutex_unlock(&mtx);
	errexit(err, "");
	
	pthread_exit(NULL);
	
	return 0;
}

void *alien(void *args) {
	int entry = -1;
	int i, err;
	pthread_t pid;
	
	err = pthread_mutex_lock(&mtx);
	errexit(err, "");
	
	outside_count++;
	
	/* wait to enter the house */
	while (house_count >= HOUSE_CAP) {
		err = pthread_cond_wait(&cond_alien_exit, &mtx);
		errexit(err, "");
	}
	
	/* get in */
	entry = house_progressive++;
	house_count++;
	outside_count--;
	err = pthread_cond_broadcast(&cond_alien_enter);
	errexit(err, "");
	
	#ifdef DEBUG
	usleep(DEBUG_PRINT_DELAY);
	printf("[%lu] I'm in (%d)\n", pthread_self(), entry);
	#endif
	
	/* shoot I'm dying, I have to get out! */
	
	/* can't get out if I'm the only one in da house */
	while (house_count <= 1) {
		err = pthread_cond_wait(&cond_alien_enter, &mtx);
		errexit(err, "");
	}
	
	/* wait till it's my turn to get out */
	while (entry > house_last_exit + 1) {
		err = pthread_cond_wait(&cond_alien_exit, &mtx);
		errexit(err, "");
	}
	
	/* finally get out */
	house_count--;
	outside_count++;
	house_last_exit = entry;
	err = pthread_cond_broadcast(&cond_alien_exit);
	errexit(err, "");
	
	#ifdef DEBUG
	usleep(DEBUG_PRINT_DELAY);
	printf("[%lu] I'm out (%d)\n", pthread_self(), entry);
	#endif
	
	/* I'll just make some children while I die */
	if (outside_count <= ALIENS_OUTSIDE_MIN) {
		
		#ifdef DEBUG
		usleep(DEBUG_PRINT_DELAY);
		printf("[%lu] Making babies\n", pthread_self());
		#endif
		
		for (i = 0; i < ALIENS_NEW; i++) {
			err = pthread_create(&pid, &attr, alien, NULL);
			errexit(err, "");
		}
	}
	
	/* I'm dead */
	outside_count--;
	
	#ifdef DEBUG
	usleep(DEBUG_PRINT_DELAY);
	printf("[%lu] I'm dead\n", pthread_self());
	#endif
	
	#ifdef DEBUG
	usleep(DEBUG_PRINT_DELAY);
	printf("house_count=%d  outside_count=%d  house_progressive=%d\n", house_count, outside_count, house_progressive);
	#endif
	
	err = pthread_mutex_unlock(&mtx);
	errexit(err, "");
	
	pthread_exit(NULL);
}

void errexit(int err, char *msg) {
	char buf[256];
	
	if (err) {
		strerror_r(err, buf, 256);
		printf("[ FATAL ] %s. %s", msg, buf);
		exit(1);
	}
}