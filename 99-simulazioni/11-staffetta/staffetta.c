#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <errno.h>

#define N_ALFA 5
#define N_BETA 4

typedef struct sharedmem_s {
	pthread_mutex_t mtx;
	short turn;
	int n_runs[2][N_ALFA];
	short field_busy;
	pthread_cond_t cond_finish;
} SharedMem;

void errexit(int err, char *msg);
int min(int *array, int size);
void athlete(short group, int group_size, int num);

pthread_mutexattr_t mtx_attr;
pthread_condattr_t cond_finish_attr;
SharedMem *shm;

int main(int argc, char **argv) {
	int shm_addr;
	int i, err;
	pid_t pid;
	
	shm_addr = shm_open("/staffetta", O_RDWR|O_CREAT, S_IRWXU);
	if (shm_addr == -1) errexit(-1, "shm_open() failed");
	
	err = ftruncate(shm_addr, sizeof(SharedMem));
	if (err != 0) errexit(-1, "ftruncate() failed");
	
	shm = (SharedMem *)mmap(NULL, sizeof(SharedMem), PROT_READ|PROT_WRITE, MAP_SHARED, shm_addr, 0);
	if (shm == NULL) errexit(-1, "error in mmap()");
	
	errexit(pthread_mutexattr_init(&mtx_attr), "cannot init mutexattr");
	errexit(pthread_mutexattr_setpshared(&mtx_attr, PTHREAD_PROCESS_SHARED), "cannot set shared mutexattr");
	errexit(pthread_mutex_init(&shm->mtx, &mtx_attr), "cannot init mutex");
	
	errexit(pthread_condattr_init(&cond_finish_attr), "cannot init condattr");
	errexit(pthread_condattr_setpshared(&cond_finish_attr, PTHREAD_PROCESS_SHARED), "cannot set shared condattr");
	errexit(pthread_cond_init(&shm->cond_finish, &cond_finish_attr), "cannot init cond");
	
	shm->field_busy = 0;
	shm->turn = 0;
	
	for (i = 0; i < N_ALFA; i++) {
		pid = fork();
		if (pid == -1) errexit(errno, "fork() failed");
		
		if (pid == 0) {
			shm->n_runs[0][i] = 0;
			athlete(0, N_ALFA, i);
			return 0;
		}
	}
	
	for (i = 0; i < N_BETA; i++) {
		pid = fork();
		if (pid == -1) errexit(errno, "fork() failed");
		
		if (pid == 0) {
			shm->n_runs[1][i] = 0;
			athlete(1, N_BETA, i);
			return 0;
		}
	}
	
	while (1) {
		pthread_mutex_lock(&shm->mtx);
		printf("turn=%d  field_busy=%d\n", shm->turn, shm->field_busy);
		pthread_mutex_unlock(&shm->mtx);
		sleep(1);
	}
	
	return 0;
}

void athlete(short group, int group_size, int num) {
	while (1) {
		errexit(pthread_mutex_lock(&shm->mtx), "mutex_lock() failed");
		
		printf("group=%d  num=%d  n_runs=%d\n", group, num, shm->n_runs[group][num]);
		
		while (group != shm->turn || shm->field_busy || min(shm->n_runs[group], group_size) < shm->n_runs[group][num]) {
			errexit(pthread_cond_wait(&shm->cond_finish, &shm->mtx), "cond_wait() failed");
		}
		
		shm->field_busy = 1;
		
		errexit(pthread_mutex_unlock(&shm->mtx), "mutex_unlock() failed");
		sleep(1);
		errexit(pthread_mutex_lock(&shm->mtx), "mutex_lock() failed");
		
		shm->n_runs[group][num]++;
		shm->field_busy = 0;
		shm->turn = !shm->turn;
		errexit(pthread_cond_broadcast(&shm->cond_finish), "cond_broadcast() failed");
		
		errexit(pthread_mutex_unlock(&shm->mtx), "mutex_unlock() failed");
	}
}

int min(int *array, int size) {
	int i, min;
	min = array[0];
	for (i = 1; i < size; i++)
		if (array[i] < min)
			min = array[i];
	return min;
}

void errexit(int err, char *msg) {
	char buf[256];
	
	if (err == -1) {
		printf("Error: %s.\n", msg);
		exit(1);
	}
	if (err) {
		strerror_r(err, buf, 256);
		printf("Error: %s: %s\n", msg, buf);
		exit(1);
	}
}
