#!/bin/bash

FILES=$(find /usr/include/linux -maxdepth 1 -iname "*f*.h")

for FILE in $FILES; do
    LINES=$(wc -l "$FILE" | cut -d' ' -f1)
    if (( $LINES >= 10 && $LINES <= 100 )); then
        #echo -n "$FILE    "
        tail -n 1 "$FILE" | wc -m
    fi
done
